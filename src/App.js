import React, { Component } from 'react';
import './App.css';
import BeersList from './components/BeersList/BeersList';

class App extends Component {

  state = {
    items: []
  };

  render() {
    return (
      <div className="App">
        <div className="App-body">
          <BeersList
            items={this.state.items}/>
        </div>

      </div>
    );
  }

  componentDidMount() {
    fetch('https://api.punkapi.com/v2/beers')
    .then(results => {
      return results.json();
    }).then (data => {
      console.log('DATA LOADED', data);
      this.setState({
        items: data
      })
    })
  }
}

export default App;

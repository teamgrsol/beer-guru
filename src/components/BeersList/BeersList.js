import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Beer from './Beer';
import './BeersList.css'

class BeersList extends Component {

  renderBeers() {
    const items = [ ...this.props.items ];

    if(items.length) {
      return items.map((item, index) => {
        return(
          <div
            key={index}
            style={{margin: '10px'}}
            >
            <Beer
              item={item} />
          </div>

        )
      });
    }
  }

  render() {
    return(
      <div className="beers-container">
        {this.renderBeers()}
      </div>

    )
  }
}


BeersList.propTypes = {
  items: PropTypes.array
}

export default BeersList;

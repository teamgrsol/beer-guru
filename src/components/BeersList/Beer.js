import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import './Beer.css';

class Beer extends Component {

  renderBeer() {
    const beer = { ...this.props.item }
    if(Object.keys(beer).length) {
      return(
        <img src={beer.image_url} className="beer-img"/>
      )
    }
  }

  render() {
    return(
      <Paper style={{padding: '10px'}}>
        {this.renderBeer()}
      </Paper>
    )
  }
}

Beer.propTypes = {
  item: PropTypes.object
}

export default Beer;
